﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Infoblock.Api.Validation.Attributes
{
    public class ValidPublishNewsRequestAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value is not DateTime request)
                return new ValidationResult("Invalid time passed");
            if (request < DateTime.UtcNow.AddMinutes(-1))
                return new ValidationResult("Publication date must be no later than the current date", new[] { nameof(request) });
            return ValidationResult.Success;
        }
    }
}