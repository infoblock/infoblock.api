﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using Infoblock.Data.Api.Requests;

namespace Infoblock.Api.Validation.Attributes
{
    public class ValidSearchNewsRequestAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (!(value is SearchNewsRequest request))
                return new ValidationResult("Invalid request passed");
            if (request.Tags != null && request.Tags.Count != request.Tags.Distinct().Count())
                return new ValidationResult("Tags should not be repeated", new[] { nameof(request.Tags) });
            if (request.AudienceTypes != null && request.AudienceTypes.Count != request.AudienceTypes.Distinct().Count())
                return new ValidationResult("Audience types should not be repeated", new[] { nameof(request.AudienceTypes) });
            if (request.Page is not > 0)
                return new ValidationResult("Page number must be greater than 0", new[] { nameof(request.Page) });
            if (request.Count is not > 0)
                return new ValidationResult("Count must be greater than 0", new[] { nameof(request.Count) });
            
            return ValidationResult.Success;
        }
    }
}