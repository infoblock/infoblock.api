﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Infoblock.Data.Api.Requests;

namespace Infoblock.Api.Validation.Attributes
{
    public class ValidCreateNewsRequestAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value is not CreateNewsRequest request)
                return new ValidationResult("Invalid request passed");
            // if (request.ReadTime is <= 0)
            //     return new ValidationResult("Reading time must be greater than 0", new[] { nameof(request.ReadTime) });
            if (request.Tags != null && request.Tags.Count != request.Tags.Distinct().Count())
                return new ValidationResult("Tags should not be repeated", new[] { nameof(request.Tags) });
            if (request.AudienceTypes != null && request.AudienceTypes.Count != request.AudienceTypes.Distinct().Count())
                return new ValidationResult("Audience types should not be repeated", new[] { nameof(request.AudienceTypes) });
            if ((request.Publish ?? false) && request.PublicationDate != null && request.PublicationDate < DateTime.UtcNow.AddMinutes(-1))
                return new ValidationResult("Publication date must be no later than the current date", new[] { nameof(request) });
            return ValidationResult.Success;
        }
    }
}