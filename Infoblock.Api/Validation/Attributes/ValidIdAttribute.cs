﻿using System.ComponentModel.DataAnnotations;
using MongoDB.Bson;

namespace Infoblock.Api.Validation.Attributes
{
    public class ValidIdAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value is not string stringId)
                return new ValidationResult("Invalid request passed");
            if (!ObjectId.TryParse(stringId, out var objectId))
               return new ValidationResult($"ID should be a valid 24 digit hex string, ID: '{stringId}' is invalid", new[] { nameof(value) });
            return ValidationResult.Success;
        }
    }
}