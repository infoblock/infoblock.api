using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Infoblock.BusinessLogic.Services;
using Infoblock.Data.Api.Responses;
using Infoblock.Data.Configuration;
using Infoblock.Data.Exceptions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Vostok.Configuration.Abstractions;


namespace Infoblock.Api.Controllers;

//[Authorize]
[ApiController]
[Route("[controller]")]
public class UsersController : ControllerBase
{
    private readonly IUsersService usersService;
    private readonly KeycloackSettings keycloackSettings;
    
    public UsersController(IUsersService usersService, IConfigurationProvider configurationProvider)
    {
        this.usersService = usersService;
        keycloackSettings = configurationProvider.Get<KeycloackSettings>();
    }
    
    [HttpGet]
    [ProducesResponseType(typeof(UsersResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(CustomException), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(CustomException), StatusCodes.Status401Unauthorized)]
    public async Task<ActionResult<UsersResponse>> SearchUsers([FromQuery] bool? enabled)
    {
        var userId = User.Claims.FirstOrDefault(x => x.Type == keycloackSettings.SubClaim)?.Value ?? "";
        var users = await usersService.GetUsers(enabled, userId);
        return Ok(users);
    }
    
    [AllowAnonymous]
    [HttpGet("test")]
    public async Task<IActionResult> Test()
    {
        return Unauthorized("тут живет 401 ошибка");
    }
    
    [HttpGet("invites")]
    [ProducesResponseType(typeof(InvitesResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(CustomException), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(CustomException), StatusCodes.Status401Unauthorized)]
    public async Task<ActionResult<InvitesResponse>> GetInvites()
    {
        var invites = await usersService.GetInvites();
        return Ok(invites);
    }
    
    [HttpPut("{userId}/ban")]
    [ProducesResponseType(typeof(CustomException), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(CustomException), StatusCodes.Status401Unauthorized)]
    public async Task<IActionResult> Ban(string userId)
    {
        if (User.Claims.FirstOrDefault(x => x.Type == keycloackSettings.SubClaim)?.Value == userId)
            return BadRequest("User can't ban himself");
        
        await usersService.ChangeUserStatus(userId, false);
        return Ok();
    }
    
    [HttpPut("{userId}/unban")]
    [ProducesResponseType(typeof(CustomException), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(CustomException), StatusCodes.Status401Unauthorized)]
    public async Task<IActionResult> Unban(string userId)
    {
        if (User.Claims.FirstOrDefault(x => x.Type == keycloackSettings.SubClaim)?.Value == userId)
            return BadRequest("User can't unban himself");
        
        await usersService.ChangeUserStatus(userId, true);
        return Ok();
    }
    
    [HttpPost("invite")]
    [ProducesResponseType(typeof(CustomException), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(CustomException), StatusCodes.Status401Unauthorized)]
    public async Task<IActionResult> SendInvite([FromQuery, Required, EmailAddress] string email)
    {
        await usersService.InviteUser(email);
        return Ok();
    }
}
