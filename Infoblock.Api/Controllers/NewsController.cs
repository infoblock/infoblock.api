using System.Linq;
using System.Threading.Tasks;
using Infoblock.Api.Validation.Attributes;
using Infoblock.BusinessLogic.Services;
using Infoblock.Data.Api.Requests;
using Infoblock.Data.Api.Responses;
using Infoblock.Data.Configuration;
using Infoblock.Data.Exceptions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using Vostok.Configuration.Abstractions;

namespace Infoblock.Api.Controllers;

//[Authorize]
[ApiController]
[Route("[controller]")]
public class NewsController : ControllerBase
{
    private readonly INewsService newsService;
    private readonly KeycloackSettings keycloackSettings;

    public NewsController(INewsService newsService, IConfigurationProvider configurationProvider)
    {
        this.newsService = newsService;
        keycloackSettings = configurationProvider.Get<KeycloackSettings>();
    }
    
    [HttpGet]
    [ProducesResponseType(typeof(NewsListResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(CustomException), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(CustomException), StatusCodes.Status401Unauthorized)]
    public async Task<ActionResult<NewsListResponse>> Search([FromQuery] SearchNewsRequest searchRequest)
    {
        var newsList = await newsService.Search(searchRequest);
        return Ok(newsList);
    }
    
    [HttpGet("{newsId}")]
    [ProducesResponseType(typeof(NewsResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(CustomException), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(CustomException), StatusCodes.Status401Unauthorized)]
    public async Task<ActionResult<NewsResponse>> GetNews([ValidId] string newsId)
    {
        var news = await newsService.GetNews(ObjectId.Parse(newsId));
        return Ok(news);
    }
    
    [HttpPost]
    [ProducesResponseType(typeof(NewsResponse), StatusCodes.Status201Created)]
    [ProducesResponseType(typeof(CustomException), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(CustomException), StatusCodes.Status401Unauthorized)]
    public async Task<ActionResult<NewsResponse>> CreateNews([FromBody, ValidCreateNewsRequest] CreateNewsRequest newsRequest)
    {
        var userId = User.Claims.FirstOrDefault(x => x.Type == keycloackSettings.SubClaim)?.Value ?? "";
        var news = await newsService.CreateNews(newsRequest, userId);
        return Created("news", news);
    }
    
    // [HttpPut("{newsId}/publish")]
    // [ProducesResponseType(typeof(CustomException), StatusCodes.Status400BadRequest)]
    // [ProducesResponseType(typeof(CustomException), StatusCodes.Status401Unauthorized)]
    // public async Task<ActionResult> PublishNews([ValidId] string newsId, [FromQuery] [ValidPublishNewsRequest] DateTime? publishTime)
    // {
    //     var userId = User.Claims.FirstOrDefault(x => x.Type == keycloackSettings.SubClaim)?.Value ?? "";
    //     await newsService.PublishNews(publishTime ?? DateTime.Now, ObjectId.Parse(newsId), userId);
    //     return Ok();
    // }
    
    [HttpPatch("{newsId}")]
    [ProducesResponseType(typeof(CustomException), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(CustomException), StatusCodes.Status401Unauthorized)]
    public async Task<ActionResult<NewsResponse>> UpdateNews([FromBody, ValidUpdateNewsRequest] UpdateNewsRequest newsRequest, [ValidId] string newsId)
    {
        var userId = User.Claims.FirstOrDefault(x => x.Type == keycloackSettings.SubClaim)?.Value ?? "";
        await newsService.UpdateNews(newsRequest, ObjectId.Parse(newsId), userId);
        return Ok();
    }
    
    [HttpDelete("{newsId}")]
    [ProducesResponseType(typeof(CustomException), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(CustomException), StatusCodes.Status401Unauthorized)]
    public async Task<ActionResult> DeleteNews([ValidId] string newsId)
    {
        await newsService.DeleteNews(ObjectId.Parse(newsId));
        return Ok();
    }
}
