using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Infoblock.BusinessLogic.Services;
using Infoblock.Data.Api.Responses;
using Infoblock.Data.Exceptions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Infoblock.Api.Controllers;

[AllowAnonymous]
[ApiController]
[Route("[controller]")]
public class AuthController : ControllerBase
{
    private readonly IUsersService usersService;
    
    public AuthController(IUsersService usersService)
    {
        this.usersService = usersService;
    }
    
    [HttpPost("login")]
    [ProducesResponseType(typeof(AuthResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(CustomException), StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<AuthResponse>> Login([FromForm, Required, EmailAddress] string email, [FromForm, Required] string password)
    {
        var loginResult = await usersService.Login(email, password);
        return Ok(loginResult);
    }
    
    [HttpPost("registration/{token}")]
    [ProducesResponseType(typeof(AuthResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(CustomException), StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<AuthResponse>> Registration([FromForm, Required] string email, [FromForm, Required] string password, Guid token)
    {
        var registrationResult = await usersService.Register(email, password, token.ToString());
        return Ok(registrationResult);
    }
    
    [HttpPost("token/refresh")]
    [ProducesResponseType(typeof(AuthResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(CustomException), StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<AuthResponse>> RefreshToken([FromForm, Required] string token)
    {
        var loginResult = await usersService.RefreshToken(token);
        return Ok(loginResult);
    }
}
