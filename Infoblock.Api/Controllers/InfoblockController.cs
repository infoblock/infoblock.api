using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Infoblock.Api.Validation.Attributes;
using Infoblock.BusinessLogic.Services;
using Infoblock.Data.Api.Requests;
using Infoblock.Data.Api.Responses;
using Infoblock.Data.Exceptions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;

namespace Infoblock.Api.Controllers;
[AllowAnonymous]
[ApiController]
[Route("[controller]/news")]
public class InfoblockController : ControllerBase //костыль
{
    private readonly INewsService newsService;

    public InfoblockController(INewsService newsService)
    {
        this.newsService = newsService;
    }
    
    [HttpGet]
    [ProducesResponseType(typeof(NewsListResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(CustomException), StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<NewsListResponse>> Search([FromQuery] SearchNewsRequest searchRequest)
    {
        var newsList = await newsService.SearchPublished(searchRequest);
        return Ok(newsList);
    }
    
    [HttpGet("{newsId}")]
    [ProducesResponseType(typeof(NewsResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(ValidationResult), StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<NewsResponse>> GetNews([ValidId] string newsId)
    {
        var news = await newsService.GetPublishedNews(ObjectId.Parse(newsId));
        return Ok(news);
    }
}
