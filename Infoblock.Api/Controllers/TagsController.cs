using System.Collections.Generic;
using System.Threading.Tasks;
using Infoblock.BusinessLogic.Services;
using Infoblock.Data.Exceptions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Infoblock.Api.Controllers;

[Authorize]
[ApiController]
[Route("[controller]")]
public class TagsController : ControllerBase
{
    private readonly ITagsService tagsService;

    public TagsController(ITagsService tagsService)
    {
        this.tagsService = tagsService;
    }

    [HttpGet]
    [ProducesResponseType(typeof(IList<string>), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(CustomException), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(CustomException), StatusCodes.Status401Unauthorized)]
    public async Task<ActionResult<IList<string>>> Search([FromQuery] string tag)
    {
        var tags = await tagsService.Search(tag);
        return Ok(tags);
    }

    [HttpPost]
    [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(CustomException), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(CustomException), StatusCodes.Status401Unauthorized)]
    public async Task<ActionResult> AddTag([FromQuery] string tag)
    {
        var tagResult = await tagsService.Add(tag);
        return Ok(tagResult);
    }

    [HttpDelete] 
    [ProducesResponseType(typeof(CustomException), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(CustomException), StatusCodes.Status401Unauthorized)]
    public async Task<ActionResult> DeleteTag([FromQuery] string tag)
    {
        await tagsService.Delete(tag);
        return Ok();
    }
}
