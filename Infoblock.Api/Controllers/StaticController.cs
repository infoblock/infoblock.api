using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Infoblock.BusinessLogic.S3;
using Infoblock.Data.Exceptions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Infoblock.Api.Controllers;

[Authorize]
[ApiController]
[Route("[controller]")]
public class StaticController : ControllerBase
{
    private readonly IS3Storage staticService;

    public StaticController(IS3Storage staticService)
    {
        this.staticService = staticService;
    }

    [HttpPost("files")]
    [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(CustomException), StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(typeof(CustomException), StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<string>> UploadFile([FromForm, Required] IFormFile file)
    {
        var fileLink = await staticService.UploadFile(file);
        return Created("static/files", fileLink);
    }
}
