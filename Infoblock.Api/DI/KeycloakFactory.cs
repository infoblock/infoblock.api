using FS.Keycloak.RestApiClient.Client;
using Infoblock.Data.Configuration;
using Vostok.Configuration.Abstractions;

namespace Infoblock.Api.DI;

internal class KeycloakFactory
{
    private readonly KeycloackSettings keycloackSettings;

    public KeycloakFactory(IConfigurationProvider configuration) => 
        keycloackSettings = configuration.Get<KeycloackSettings>();

    public KeycloakHttpClient GetClient()
    {
        var httpClient = new KeycloakHttpClient(keycloackSettings.BaseUrl, keycloackSettings.Login, keycloackSettings.Password);
        return httpClient;
    }
}
