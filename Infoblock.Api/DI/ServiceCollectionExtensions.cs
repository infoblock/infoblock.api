﻿using System.Linq;
using Infoblock.BusinessLogic.Mongo;
using Infoblock.BusinessLogic.Repositories;
using Infoblock.BusinessLogic.S3;
using Infoblock.BusinessLogic.Services;
using Infoblock.BusinessLogic.Spam;
using Infoblock.Data.Configuration;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Vostok.Configuration;
using Vostok.Configuration.Abstractions;
using Vostok.Configuration.Sources.Json;
using Vostok.Logging.Abstractions;
using Vostok.Logging.Console;
using Vostok.Logging.File;
using Vostok.Logging.File.Configuration;

namespace Infoblock.Api.DI;

internal static class ServiceCollectionExtensions
{
    private const string FrontSpecificOrigins = "_frontSpecificOrigins";

    public static IServiceCollection AddServiceCors(this IServiceCollection services)
    {
        var serviceProvider = services.BuildServiceProvider();
        var serviceProperties = serviceProvider.GetRequiredService<IConfigurationProvider>().Get<ServiceProperties>();

        services
            .AddCors(option => option
                .AddPolicy(FrontSpecificOrigins, builder => builder.WithOrigins(serviceProperties.Origins.ToArray())
                    .AllowAnyMethod()
                    .AllowAnyHeader()));
        return services;
    }
    
    public static IServiceCollection AddServiceAuthentication(this IServiceCollection services)
    {
        var serviceProvider = services.BuildServiceProvider();
        var keycloackSettings = serviceProvider.GetRequiredService<IConfigurationProvider>().Get<KeycloackSettings>();
        
        services.AddAuthentication(options =>
            {
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                options.RequireHttpsMetadata = false;
                options.Authority = keycloackSettings.Authority;
                options.Audience = keycloackSettings.Audience;
            });
        return services;
    }
    
    public static IServiceCollection AddServiceSwaggerDocument(this IServiceCollection services) =>
        services.AddSwaggerDocument(doc =>
        {
            doc.Title = "Infoblock.Api";
            doc.AddSecurity("Bearer", Enumerable.Empty<string>(), new NSwag.OpenApiSecurityScheme
            {
                Type = NSwag.OpenApiSecuritySchemeType.ApiKey,
                Name = "Authorization",
                In = NSwag.OpenApiSecurityApiKeyLocation.Header,
                Description = "Type into the textbox: Bearer {your JWT token}."
            });
        });

    public static void AddSettings(this IServiceCollection services)
    {
        var provider = new ConfigurationProvider();
        
        provider.SetupSourceFor<ServiceProperties>(new JsonFileSource("api.settings/ServiceProperties.json"));
        provider.SetupSourceFor<StorageSettings>(new JsonFileSource("api.settings/StorageSettings.json"));
        provider.SetupSourceFor<MongoSettings>(new JsonFileSource("api.settings/MongoSettings.json"));
        provider.SetupSourceFor<KeycloackSettings>(new JsonFileSource("api.settings/KeycloackSettings.json"));
        provider.SetupSourceFor<SmtpSettings>(new JsonFileSource("api.settings/SmtpSettings.json"));
        
        services.AddSingleton<IConfigurationProvider>(provider);
    }

    public static void AddSystemServices(this IServiceCollection services) => services
        .AddDistributedMemoryCache()
        .AddSingleton<IS3Storage, YandexStorage>()
        .AddSingleton<ISpamService, SmtpService>()
        .AddSingleton<IMongoProvider, MongoProvider>()
        .AddSingleton<IUsersService, UsersService>()
        .AddSingleton<INewsService, NewsService>()
        .AddSingleton<INewsRepository, NewsRepository>()
        .AddSingleton<ITagsService, TagsService>()
        .AddSingleton<ITagsRepository, TagsRepository>();

    public static void AddExternalServices(this IServiceCollection services)
    {
        services
            .AddSingleton<ILog>(new CompositeLog(new ConsoleLog(), new FileLog(new FileLogSettings())));
        services
            .AddSingleton<S3ClientFactory>()
            .TryAddSingleton(x => x.GetRequiredService<S3ClientFactory>().GetClient());
        services
            .AddSingleton<MongoFactory>()
            .TryAddSingleton(x => x.GetRequiredService<MongoFactory>().GetClient());
        services
            .AddSingleton<KeycloakFactory>()
            .TryAddSingleton(x => x.GetRequiredService<KeycloakFactory>().GetClient());
        services
            .AddSingleton<SmtpClientFactory>()
            .TryAddSingleton(x => x.GetRequiredService<SmtpClientFactory>().GetClient());
    }
}
