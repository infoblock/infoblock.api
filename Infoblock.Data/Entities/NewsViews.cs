﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Infoblock.Data.Entities;

public class NewsViews
{
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string Id { get; set; }
    [BsonRepresentation(BsonType.ObjectId)]
    public string NewsId { get; set; }
    public int Count { get; set; }
}
