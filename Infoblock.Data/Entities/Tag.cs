﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Infoblock.Data.Entities;

public class Tag
{
    public Tag(ObjectId Id, string Name, int Count)
    {
        this.Id = Id;
        this.Name = Name;
        this.Count = Count;
    }
    
    public Tag()
    {
        
    }

    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public ObjectId Id { get; set; }
    [BsonElement("Name", Order = 1)]
    public string Name { get; set; }
    public int Count { get; set; }
}
