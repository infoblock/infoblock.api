namespace Infoblock.Data.Api.Requests;

public class SearchNewsRequest
{
    /// <summary>
    /// Search query, Default: null
    /// </summary>
    public string? Query { get; set; }
    /// <summary>
    /// News author, Default: null
    /// </summary>
    public string? Author { get; set; }
    /// <summary>
    /// Default: null
    /// </summary>
    public List<string>? Tags { get; set; }
    /// <summary>
    /// Default: null
    /// </summary>
    public List<string>? AudienceTypes { get; set; }
    /// <summary>
    /// Default: null
    /// </summary>
    public List<string>? Locations { get; set; }
    /// <summary>
    /// Default: Search any type 
    /// </summary>
    public NewsType? NewsType { get; set; }
    /// <summary>
    /// Default: 1
    /// </summary>
    public int? Page { get; set; } = 1;
    /// <summary>
    /// Default: 50
    /// </summary>
    public int? Count { get; set; } = 50;
    /// <summary>
    /// Default: PublicationDate
    /// </summary>
    public SortField? SortField { get; set; }
    /// <summary>
    /// Default: Ascending 
    /// </summary>
    public SortDirection? SortDirection { get; set; }
}

public enum NewsType
{
    Published = 1,
    NotPublished = 2,
    Draft = 3
}

public enum SortDirection
{
    Ascending  = 1,
    Descending  = -1
}

public enum SortField 
{
    ReadTime,
    CreateDate,
    PublicationDate,
    EditeDate
}
