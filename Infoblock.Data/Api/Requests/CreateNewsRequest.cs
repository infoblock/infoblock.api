﻿using System.ComponentModel.DataAnnotations;

namespace Infoblock.Data.Api.Requests;

public class CreateNewsRequest
{
    [Required]
    public string Title { get; set; }
    public string? Description { get; set; }
    public string? Avatar { get; set; }
    public string? Body { get; set; }
    public int? ReadTime { get; set; }
    public string? Link { get; set; }
    public string? FeatureFlag { get; set; }
    public string? Author { get; set; }
    public List<string>? Tags { get; set; }
    public List<string>? AudienceTypes { get; set; }
    public List<string>? Locations { get; set; }
    public bool? Publish { get; set; }
    public DateTime? PublicationDate { get; set; }
}