﻿namespace Infoblock.Data.Api.Responses;

public class AuthResponse
{
    public string AccessToken { get; set; }
    public int ExpiresIn { get; set; }
    public string RefreshToken { get; set; }
    public int RefreshExpiresIn { get; set; }
    public string SessionState { get; set; }
}