﻿namespace Infoblock.Data.Api.Responses;

public class UsersResponse
{
    public UsersResponse(IList<UserResponse> foundEntities, long totalCount)
    {
        FoundEntities = foundEntities;
        TotalCount = totalCount;
    }

    public UsersResponse()
    {
    }

    public IList<UserResponse> FoundEntities { get; set; }

    public long TotalCount { get; set; }
}