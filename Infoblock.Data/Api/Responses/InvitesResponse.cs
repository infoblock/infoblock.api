﻿namespace Infoblock.Data.Api.Responses;

public class InvitesResponse
{
    public InvitesResponse(IList<InviteResponse> foundEntities, long totalCount)
    {
        FoundEntities = foundEntities;
        TotalCount = totalCount;
    }

    public InvitesResponse()
    {
    }

    public IList<InviteResponse> FoundEntities { get; set; }

    public long TotalCount { get; set; }
}