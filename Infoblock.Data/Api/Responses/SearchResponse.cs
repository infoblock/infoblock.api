﻿namespace Infoblock.Data.Api;

public class SearchResponse<TEntity>
{
    public SearchResponse()
    {
        FoundEntities = new List<TEntity>();
        TotalCount = 0;
    }

    public SearchResponse(IList<TEntity> foundEntities, long totalCount)
    {
        FoundEntities = foundEntities;
        TotalCount = totalCount;
    }

    public IList<TEntity> FoundEntities { get; set; }

    public long TotalCount { get; set; }
}
