﻿namespace Infoblock.Data.Api.Responses;

public class NewsResponse
{
    public string Id { get; set; }
    public string Title { get; set; }
    public string Link { get; set; }
    public string Description { get; set; }
    public string FeatureFlag { get; set; }
    public bool IsActive { get; set; }
    public int PreviewsCount { get; set; }
    public int ViewsCount { get; set; }
    public string Avatar { get; set; }
    public string Body { get; set; }
    public string Author { get; set; }
    public int ReadTime { get; set; }
    public List<string> Tags { get; set; }
    public List<string> AudienceTypes { get; set; }
    public List<string> Locations { get; set; }
    public DateTime CreateDate { get; set; }
    public DateTime? PublicationDate { get; set; }
    public DateTime? EditeDate { get; set; }
}