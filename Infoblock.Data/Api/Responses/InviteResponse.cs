﻿namespace Infoblock.Data.Api.Responses;

public class InviteResponse
{
    public string Email { get; set; }
    public DateTime InviteDate { get; set; }
}