﻿namespace Infoblock.Data.Api.Responses;

public class UserResponse
{
    public string Id { get; set; }
    public string Email { get; set; }
    public DateTime CreateDate { get; set; }
    public int PublicationsCount { get; set; }
    public bool Enabled { get; set; }
}