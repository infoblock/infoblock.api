﻿namespace Infoblock.Data.Api.Responses;

public class NewsListResponse
{
    public NewsListResponse(IList<NewsResponse> foundEntities, long totalCount)
    {
        FoundEntities = foundEntities;
        TotalCount = totalCount;
    }

    public NewsListResponse()
    {
    }

    public IList<NewsResponse> FoundEntities { get; set; }

    public long TotalCount { get; set; }
}