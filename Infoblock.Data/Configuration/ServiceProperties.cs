﻿namespace Infoblock.Data.Configuration;

public class ServiceProperties
{
    public List<string> Origins { get; set; }
    public string FrontRegisterUrl { get; set; }
}