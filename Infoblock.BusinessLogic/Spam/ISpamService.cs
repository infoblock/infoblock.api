﻿namespace Infoblock.BusinessLogic.Spam;

public interface ISpamService
{
    Task SendMessage(string email , string subject, string body, bool isHtml);
}