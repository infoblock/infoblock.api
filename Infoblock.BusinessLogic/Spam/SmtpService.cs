﻿using System.Net.Mail;
using Infoblock.Data.Configuration;
using Infoblock.Data.Exceptions;
using Vostok.Configuration.Abstractions;
using Vostok.Logging.Abstractions;

namespace Infoblock.BusinessLogic.Spam;

public class SmtpService : ISpamService
{
    private readonly ILog log;
    private readonly SmtpSettings smtpSettings;
    private readonly SmtpClient smtpClient;

    public SmtpService(ILog log, IConfigurationProvider configurationProvider, SmtpClient smtpClient)
    {
        this.log = log;
        this.smtpClient = smtpClient;
        smtpSettings = configurationProvider.Get<SmtpSettings>();
    }

    public Task SendMessage(string email, string subject, string body, bool isHtml)
    {
        try
        {
            var message = new MailMessage
            {
                From = new MailAddress(smtpSettings.EmailFrom),
                Subject = subject,
                IsBodyHtml = isHtml,
                Body = body
            };
            message.To.Add(email);
            
            smtpClient.SendAsync(message, Guid.NewGuid());
            log.Info($"Message to the email: '{email}' has been sent successfully");
        }
        catch (Exception)
        {
            log.Error($"Email: '{email}' send error");
            throw new CustomException("Email send error", 500);
        }
        return Task.CompletedTask;
    }
}

 