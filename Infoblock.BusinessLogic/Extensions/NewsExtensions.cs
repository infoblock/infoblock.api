﻿using Infoblock.Data.Api.Responses;
using Infoblock.Data.Entities;

namespace Infoblock.BusinessLogic.Extensions;

public static class NewsExtensions
{
    public static bool IsPublished(this News news) =>
        news.PublicationDate < DateTime.UtcNow;
    
    public static bool IsPublished(this NewsResponse news) =>
        news.PublicationDate < DateTime.UtcNow;
}