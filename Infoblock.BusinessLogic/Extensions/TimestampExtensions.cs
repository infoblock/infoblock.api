﻿namespace Infoblock.BusinessLogic.Extensions;

public static class TimestampExtensions
{
    public static DateTime ToDateTime(this long? timestamp) =>
        DateTimeOffset.FromUnixTimeMilliseconds(timestamp ?? 0L).DateTime;
    
    public static TimeSpan ToTimeSpan(this long? timestamp) =>
        TimeSpan.Parse(timestamp.ToString() ?? "");

    public static long ToTimestamp(this DateTime dateTime) =>
        (long)dateTime.Subtract(DateTime.UnixEpoch).TotalSeconds;
}