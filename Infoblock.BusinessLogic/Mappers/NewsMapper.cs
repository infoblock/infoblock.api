﻿using Infoblock.Data.Api.Requests;
using Infoblock.Data.Api.Responses;
using Infoblock.Data.Entities;
using MongoDB.Bson;

namespace Infoblock.BusinessLogic.Mappers;

public static class NewsMapper
{
    public static News ToDbEntity(this CreateNewsRequest createRequest, string userId)
    {
        var news = new News();
        news.Id = ObjectId.GenerateNewId();
        news.Title = createRequest.Title;
        news.CreateDate = DateTime.UtcNow;
        news.IsActive = createRequest.Publish ?? false;
        news.CreaterId = userId;
        news.Avatar = createRequest.Avatar;
        news.Body = createRequest.Body;
        news.ReadTime = createRequest.ReadTime ?? 0;
        news.Description = createRequest.Description;
        news.Link = createRequest.Link;
        news.FeatureFlag = createRequest.FeatureFlag;
        news.Author = createRequest.Author;
        news.Tags = createRequest.Tags;
        news.Locations = createRequest.Locations;
        news.AudienceTypes = createRequest.AudienceTypes;
        news.PublicationDate = createRequest.PublicationDate;
        return news;
    }
    
    public static NewsResponse ToResponse(this News newsEntity) =>
        new NewsResponse
        {
            Id = newsEntity.Id.ToString(),
            Title = newsEntity.Title,
            Avatar = newsEntity.Avatar,
            Body = newsEntity.Body,
            Author = newsEntity.Author,
            ReadTime = newsEntity.ReadTime ?? 0,
            Tags = newsEntity.Tags,
            AudienceTypes = newsEntity.AudienceTypes,
            CreateDate = newsEntity.CreateDate,
            PublicationDate = newsEntity.PublicationDate,
            EditeDate = newsEntity.EditeDate,
            IsActive = newsEntity.IsActive,
            PreviewsCount = newsEntity.PreviewsCount,
            ViewsCount = newsEntity.ViewsCount,
            Link = newsEntity.Link,
            Description = newsEntity.Description,
            Locations = newsEntity.Locations,
            FeatureFlag = newsEntity.FeatureFlag
        };
}