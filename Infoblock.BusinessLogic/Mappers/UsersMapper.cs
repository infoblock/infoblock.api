﻿using FS.Keycloak.RestApiClient.Model;
using Infoblock.BusinessLogic.Extensions;
using Infoblock.Data.Api.Responses;

namespace Infoblock.BusinessLogic.Mappers;

public static class UsersMapper
{
    public static UserResponse ToResponse(this UserRepresentation keycloackUser, int publicationsCount = 0) =>
        new UserResponse()
        {
            Id = keycloackUser.Id,
            Email = keycloackUser.Email,
            CreateDate = keycloackUser.CreatedTimestamp.ToDateTime(),
            PublicationsCount = publicationsCount,
            Enabled = keycloackUser.Enabled ?? false
        };
    
    public static InviteResponse ToInviteResponse(this UserRepresentation keycloackUser) =>
        new InviteResponse()
        {
            Email = keycloackUser.Email,
            InviteDate = keycloackUser.CreatedTimestamp.ToDateTime(),
        };
}