﻿using Infoblock.Data.Api;
using Infoblock.Data.Api.Requests;
using Infoblock.Data.Entities;
using MongoDB.Bson;

namespace Infoblock.BusinessLogic.Repositories;


public interface INewsRepository
{
    Task<SearchResponse<News>> Search(SearchNewsRequest searchRequest);
    Task<int> GetUserNewsCount(string userId);
    Task<News> Get(ObjectId id);
    Task<News> Create(CreateNewsRequest createRequest, string userId);
    Task Publish(DateTime publishTime, ObjectId id, string userId);
    Task Update(UpdateNewsRequest news, ObjectId id, string userId);
    Task Delete(ObjectId id);
    
    //костыль
    Task AddView(News news);
    Task AddPreviews(List<ObjectId> newsIds);

}

