﻿using Infoblock.Data.Api;
using Infoblock.Data.Entities;
using MongoDB.Bson;

namespace Infoblock.BusinessLogic.Repositories;

public interface ITagsRepository
{
    Task<SearchResponse<Tag>> Search(string? tagName);
    Task<Tag?> TryGet(string tag);
    Task<string> Add(string tagName);
    Task UpdateCounter(ObjectId tagId, int count);
    Task Delete(ObjectId tagId);
}

