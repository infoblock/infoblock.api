﻿using Infoblock.BusinessLogic.Mappers;
using Infoblock.BusinessLogic.Mongo;
using Infoblock.Data.Api;
using Infoblock.Data.Api.Requests;
using Infoblock.Data.Configuration;
using Infoblock.Data.Entities;
using MongoDB.Bson;
using MongoDB.Driver;
using Vostok.Configuration.Abstractions;
using Vostok.Logging.Abstractions;
using SortDirection = Infoblock.Data.Api.Requests.SortDirection;

namespace Infoblock.BusinessLogic.Repositories;


public class NewsRepository : INewsRepository
{
    private readonly IMongoProvider mongoProvider;
    private readonly IMongoCollection<News> newsCollection;
    private readonly ILog log;
    private const string RegexOption = "i"; 
    
    public NewsRepository(IMongoClient mongoClient, IConfigurationProvider configurationProvider, IMongoProvider mongoProvider, ILog log)
    {
        this.mongoProvider = mongoProvider;
        this.log = log;
        var mongoSettings = configurationProvider.Get<MongoSettings>();
        newsCollection = mongoClient.GetDatabase(mongoSettings.Database).GetCollection<News>(mongoSettings.NewsCollection);
    }

    public async Task<int> GetUserNewsCount(string userId)
    {
        var filter = new ExpressionFilterDefinition<News>(news => news.CreaterId == userId);
        var count = await mongoProvider.GetCount(newsCollection, filter);
        return (int)count;
    }

    public async Task<News> Get(ObjectId id)
    {
        var filter = new ExpressionFilterDefinition<News>(news => news.Id == id);
        var newsEntity = await mongoProvider.Get(newsCollection, filter);
        return newsEntity;
    }

    public async Task<News> Create(CreateNewsRequest createRequest, string userId)
    {
        var newsEntity = createRequest.ToDbEntity(userId);
        if ((createRequest.Publish ?? false) && createRequest.PublicationDate == null)
            newsEntity.PublicationDate = DateTime.UtcNow;
        await mongoProvider.Write(newsCollection, newsEntity);
        log.Info($"News with id: '{newsEntity.Id}' created by user: '{userId}'.");
        return newsEntity;
    }

    public async Task Publish(DateTime publishTime, ObjectId id, string userId)
    {
        var filter = new ExpressionFilterDefinition<News>(news => news.Id == id && !news.IsActive);

        var patchRequest = Builders<News>.Update
            .Set(x => x.IsActive, true)
            .Set(x => x.PublicationDate, publishTime)
            .Set(x => x.EditorId, userId)
            .Set(x => x.EditeDate, DateTime.UtcNow);

        await mongoProvider.Update(newsCollection, filter, patchRequest);
    }

    public async Task Update(UpdateNewsRequest updateRequest, ObjectId id, string userId)
    {
        var filter = new ExpressionFilterDefinition<News>(news => news.Id == id);
        
        var patchRequest = Builders<News>.Update
            .Set(x => x.EditorId, userId)
            .Set(x => x.EditeDate, DateTime.UtcNow);
        if (updateRequest.Title != null)
            patchRequest = patchRequest.Set(x => x.Title, updateRequest.Title);
        if (updateRequest.Link != null)
            patchRequest = patchRequest.Set(x => x.Link, updateRequest.Link);
        if (updateRequest.FeatureFlag != null)
            patchRequest = patchRequest.Set(x => x.FeatureFlag, updateRequest.FeatureFlag);
        if (updateRequest.Description != null)
            patchRequest = patchRequest.Set(x => x.Description, updateRequest.Description);
        if (updateRequest.Avatar != null)
            patchRequest = patchRequest.Set(x => x.Avatar, updateRequest.Avatar);
        if (updateRequest.Body != null)
            patchRequest = patchRequest.Set(x => x.Body, updateRequest.Body);
        if (updateRequest.ReadTime != null)
            patchRequest = patchRequest.Set(x => x.ReadTime, updateRequest.ReadTime);
        if (updateRequest.Author != null)
            patchRequest = patchRequest.Set(x => x.Author, updateRequest.Author);
        if (updateRequest.Tags != null)
            patchRequest = patchRequest.Set(x => x.Tags, updateRequest.Tags);
        if (updateRequest.AudienceTypes != null)
            patchRequest = patchRequest.Set(x => x.AudienceTypes, updateRequest.AudienceTypes);
        if (updateRequest.Locations != null)
            patchRequest = patchRequest.Set(x => x.Locations, updateRequest.Locations);
        if (updateRequest.Publish != null)
            patchRequest = patchRequest.Set(x => x.IsActive, updateRequest.Publish);
        if (updateRequest.PublicationDate != null)
            patchRequest = patchRequest.Set(x => x.PublicationDate, updateRequest.PublicationDate);
        if (updateRequest.Publish != null)
            patchRequest = patchRequest.Set(x => x.IsActive, updateRequest.Publish);
        if (updateRequest.Publish != null)
            patchRequest = patchRequest.Set(x => x.IsActive, updateRequest.Publish);
        patchRequest = patchRequest.Set(x => x.EditorId, userId);
        patchRequest = patchRequest.Set(x => x.EditeDate, DateTime.UtcNow);
        if (updateRequest.Publish ?? false)
            patchRequest = patchRequest.Set(x => x.PublicationDate, updateRequest.PublicationDate ?? DateTime.UtcNow);
        log.Info($"News with id: '{id}' updated by user: '{userId}'.");
        await mongoProvider.Update(newsCollection, filter, patchRequest);
    }

    public async Task Delete(ObjectId id)
    {
        var filter = new ExpressionFilterDefinition<News>(news => news.Id == id);
        log.Info($"News with id: '{id}' deleted.");
        await mongoProvider.Remove(newsCollection, filter);
    }
    
    public async Task<SearchResponse<News>> Search(SearchNewsRequest searchRequest)
    {
        var filter = CreateFilter(searchRequest);

        var sortField = searchRequest.SortField?.ToString() ?? SortField.PublicationDate.ToString();
        var sort = searchRequest.SortDirection is SortDirection.Ascending 
            ? Builders<News>.Sort.Ascending(sortField) 
            : Builders<News>.Sort.Descending(sortField);
        
        var skip = (searchRequest.Page - 1) * searchRequest.Count;
        var limit = searchRequest.Count;

        var result = await mongoProvider.Search(newsCollection, filter, sort, skip, limit);
        return result;
    }

    private static FilterDefinition<News> CreateFilter(SearchNewsRequest searchRequest)
    {
        var typeFilter = Builders<News>.Filter.Empty;
        var queryFilter = Builders<News>.Filter.Empty;
        var authorFilter = Builders<News>.Filter.Empty;
        var tagsFilter = Builders<News>.Filter.Empty;
        var audienceTypesFilter = Builders<News>.Filter.Empty;
        var locations = Builders<News>.Filter.Empty;

        typeFilter = searchRequest.NewsType switch
        {
            NewsType.Draft => Builders<News>.Filter.Eq(x => x.IsActive, false),
            NewsType.Published => Builders<News>.Filter.And(Builders<News>.Filter.Lt(x => x.PublicationDate, DateTime.UtcNow), Builders<News>.Filter.Eq(x => x.IsActive, true)),
            NewsType.NotPublished => Builders<News>.Filter.Gt(x => x.PublicationDate, DateTime.UtcNow),
            _ => typeFilter
        };

        if (searchRequest.Query != null)
            queryFilter = Builders<News>.Filter.Or(
                Builders<News>.Filter.Regex(x => x.Title, new BsonRegularExpression(searchRequest.Query, RegexOption)),
                Builders<News>.Filter.Regex(x => x.Body, new BsonRegularExpression(searchRequest.Query, RegexOption)),
                Builders<News>.Filter.Regex(x => x.Description, new BsonRegularExpression(searchRequest.Query, RegexOption)));

        if (searchRequest.Author != null)
            authorFilter = Builders<News>.Filter.Regex(x => x.Author, new BsonRegularExpression(searchRequest.Author, RegexOption));

        if (searchRequest.Tags?.Count > 0)
            tagsFilter = Builders<News>.Filter.All(x => x.Tags, searchRequest.Tags);

        if (searchRequest.AudienceTypes?.Count > 0)
            audienceTypesFilter = Builders<News>.Filter.All(x => x.AudienceTypes, searchRequest.AudienceTypes);
        
        if (searchRequest.Locations?.Count > 0)
            locations = Builders<News>.Filter.All(x => x.Locations, searchRequest.Locations);

        var filter = typeFilter & queryFilter & authorFilter & tagsFilter & audienceTypesFilter & locations;
        return filter;
    }
    
    //костыль
    public async Task AddView(News news)
    {
        var filter = new ExpressionFilterDefinition<News>(x => x.Id == news.Id);
        var patchRequest = Builders<News>.Update.Inc(x => x.ViewsCount, 1);

        await mongoProvider.Update(newsCollection, filter, patchRequest);
    }

    public async Task AddPreviews(List<ObjectId> newsIds)
    {
        var filter = new ExpressionFilterDefinition<News>(x => newsIds.Contains(x.Id));
        var patchRequest = Builders<News>.Update.Inc(x => x.PreviewsCount, 1);

        await mongoProvider.BatchUpdate(newsCollection, filter, patchRequest);
    }
}

