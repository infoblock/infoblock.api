﻿using Infoblock.BusinessLogic.Mongo;
using Infoblock.Data.Api;
using Infoblock.Data.Configuration;
using MongoDB.Bson;
using MongoDB.Driver;
using Vostok.Configuration.Abstractions;
using Tag = Infoblock.Data.Entities.Tag;

namespace Infoblock.BusinessLogic.Repositories;


public class TagsRepository : ITagsRepository
{
    private readonly IMongoProvider mongoProvider;
    private readonly IMongoCollection<Tag> tagsCollection;
    private const string SortField = "Count"; 
    private const string RegexOption = "i"; 
    
    public TagsRepository(IMongoClient mongoClient, IConfigurationProvider configurationProvider, IMongoProvider mongoProvider)
    {
        this.mongoProvider = mongoProvider;
        var mongoSettings = configurationProvider.Get<MongoSettings>();
        tagsCollection = mongoClient.GetDatabase(mongoSettings.Database).GetCollection<Tag>(mongoSettings.TagsCollection);
    }

    public async Task<SearchResponse<Tag>> Search(string? tagName)
    {
        var filter = Builders<Tag>.Filter.Empty;
        if (tagName != null)
            filter = Builders<Tag>.Filter.Regex(x => x.Name, new BsonRegularExpression(tagName, RegexOption));
        var sort = Builders<Tag>.Sort.Descending(SortField);
        var tags = await mongoProvider.Search(tagsCollection, filter, sort);
        return tags;
    }

    public async Task<Tag?> TryGet(string tagName)
    {
        var filter = new ExpressionFilterDefinition<Tag>(x => x.Name == tagName);
        var tag = await mongoProvider.TryGet(tagsCollection, filter);
        return tag;
    }

    public async Task<string> Add(string tagName)
    {
        var tag = new Tag(ObjectId.GenerateNewId(), tagName, 1);
        await mongoProvider.Write(tagsCollection, tag);
        return tag.Name;
    }

    public async Task UpdateCounter(ObjectId tagId, int count)
    {
        var filter = new ExpressionFilterDefinition<Tag>(x => x.Id == tagId);
        var patchRequest = Builders<Tag>.Update.Set(x => x.Count, count);
        await mongoProvider.Update(tagsCollection, filter, patchRequest);
    }

    public async Task Delete(ObjectId tagId)
    {
        var filter = new ExpressionFilterDefinition<Tag>(x => x.Id == tagId); 
        await mongoProvider.Remove(tagsCollection, filter);
    }
}

