﻿using Infoblock.Data.Api;
using MongoDB.Driver;

namespace Infoblock.BusinessLogic.Mongo;

public interface IMongoProvider
{
    Task<TEntity> Get<TEntity>(IMongoCollection<TEntity> mongoCollection, ExpressionFilterDefinition<TEntity> filter);
    Task<TEntity> TryGet<TEntity>(IMongoCollection<TEntity> mongoCollection, ExpressionFilterDefinition<TEntity> filter);
    Task Write<TEntity>(IMongoCollection<TEntity> mongoCollection, TEntity writeRequest);
    Task Patch<TEntity>(IMongoCollection<TEntity> mongoCollection, TEntity entity, ExpressionFilterDefinition<TEntity> filter);
    Task Update<TEntity>(IMongoCollection<TEntity> mongoCollection, ExpressionFilterDefinition<TEntity> filter, UpdateDefinition<TEntity> updateRequest);
    Task BatchUpdate<TEntity>(IMongoCollection<TEntity> mongoCollection, ExpressionFilterDefinition<TEntity> filter, UpdateDefinition<TEntity> updateRequest);
    Task Remove<TEntity>(IMongoCollection<TEntity> mongoCollection, ExpressionFilterDefinition<TEntity> filter);
    Task<SearchResponse<TEntity>> Search<TEntity>(IMongoCollection<TEntity> mongoCollection, FilterDefinition<TEntity> filter, SortDefinition<TEntity> sort, int? skip = 0, int? limit = int.MaxValue);
    Task<long> GetCount<TEntity>(IMongoCollection<TEntity> mongoCollection, ExpressionFilterDefinition<TEntity> filter);
}