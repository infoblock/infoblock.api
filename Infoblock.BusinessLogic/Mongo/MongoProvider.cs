﻿using Infoblock.Data.Api;
using Infoblock.Data.Exceptions;
using MongoDB.Driver;
using Vostok.Logging.Abstractions;

namespace Infoblock.BusinessLogic.Mongo;

public class MongoProvider : IMongoProvider
{
    private readonly ILog log;

    public MongoProvider(ILog log)
    {
        this.log = log;
    }

    public async Task<TEntity> Get<TEntity>(IMongoCollection<TEntity> mongoCollection, ExpressionFilterDefinition<TEntity> filter)
    {
        try 
        {
            var result = await mongoCollection.Find(filter).FirstOrDefaultAsync();
            if (result == null)
                throw new CustomException($"Entity '{typeof(TEntity).Name}' not found", 404);
            return result;
        }
        catch (Exception e) 
        {
            if (e is CustomException) throw;
            log.Error($"Error find entity: {e.Message}");
            throw new MongoException("Error find entity", e);
        }
    }

    public async Task<TEntity> TryGet<TEntity>(IMongoCollection<TEntity> mongoCollection, ExpressionFilterDefinition<TEntity> filter)
    {
        try 
        {
            var result = await mongoCollection.Find(filter).FirstOrDefaultAsync();
            return result;
        }
        catch (Exception e) 
        {
            if (e is CustomException) throw;
            log.Error($"Error find entity: {e.Message}");
            throw new MongoException("Error find entity", e);
        }
    }

    public async Task Write<TEntity>(IMongoCollection<TEntity> mongoCollection, TEntity entity)
    { 
        try 
        {
            await mongoCollection.InsertOneAsync(entity);
        }
        catch (Exception e) 
        {
            log.Error($"Error write entity: {entity}, {e.Message}");
            throw new MongoException("Error write entity", e);
        }
        
    }

    public async Task Patch<TEntity>(IMongoCollection<TEntity> mongoCollection, TEntity entity, ExpressionFilterDefinition<TEntity> filter)
    {
        try
        {
            await mongoCollection.ReplaceOneAsync(filter, entity);
        }
        catch (Exception e) 
        {
            log.Error($"Error patch entity: {entity}, {e.Message}");
            throw new MongoException("Error patch entity", e);
        }
    }

    public async Task Update<TEntity>(IMongoCollection<TEntity> mongoCollection, ExpressionFilterDefinition<TEntity> filter, UpdateDefinition<TEntity> updateRequest)
    {
        try
        {
            await mongoCollection.UpdateOneAsync(filter, updateRequest);
        }
        catch (Exception e) 
        {
            log.Error($"Error update entity: {updateRequest}, {e.Message}");
            throw new MongoException("Error update entity", e);
        }
    }

    public async Task BatchUpdate<TEntity>(IMongoCollection<TEntity> mongoCollection, ExpressionFilterDefinition<TEntity> filter, UpdateDefinition<TEntity> updateRequest)
    {
        try
        {
            await mongoCollection.UpdateManyAsync(filter, updateRequest);
        }
        catch (Exception e) 
        {
            log.Error($"Error update entities: {updateRequest}, {e.Message}");
            throw new MongoException("Error update entity", e);
        }
    }

    public async Task Remove<TEntity>(IMongoCollection<TEntity> mongoCollection, ExpressionFilterDefinition<TEntity> filter)
    {
        try 
        {
            await mongoCollection.DeleteOneAsync(filter);
        }
        catch (Exception e) 
        {
            log.Error($"Error deleting entity: {e.Message}");
            throw new MongoException("Error deleting entity", e);
        }
    }

    public async Task<SearchResponse<TEntity>> Search<TEntity>(IMongoCollection<TEntity> mongoCollection, FilterDefinition<TEntity> filter, SortDefinition<TEntity> sort, int? skip, int? limit)
    {
        try 
        {
            var result = await mongoCollection.Find(filter).Sort(sort).Skip(skip).Limit(limit).ToListAsync();
            var totalCount = await mongoCollection.CountDocumentsAsync(filter);
            return new SearchResponse<TEntity>(result, totalCount);
        }
        catch (Exception e) 
        {
            log.Error($"Search error: {e.Message}");
            throw new MongoException("Search error", e);
        }
    }

    public async Task<long> GetCount<TEntity>(IMongoCollection<TEntity> mongoCollection, ExpressionFilterDefinition<TEntity> filter)
    {
        try
        {
            var count = await mongoCollection.CountDocumentsAsync(filter);
            return count;
        }
        catch (Exception e) 
        {
            log.Error($"Get count error: {e.Message}");
            throw new MongoException("Get count error", e);
        }
    }
}