﻿using Infoblock.BusinessLogic.Extensions;
using Infoblock.BusinessLogic.Mappers;
using Infoblock.BusinessLogic.Repositories;
using Infoblock.Data.Api.Requests;
using Infoblock.Data.Api.Responses;
using Infoblock.Data.Exceptions;
using MongoDB.Bson;

namespace Infoblock.BusinessLogic.Services;

public class NewsService : INewsService
{
    private readonly INewsRepository newsRepository;

    public NewsService(INewsRepository newsRepository)
    {
        this.newsRepository = newsRepository;
    }

    public async Task<NewsListResponse> Search(SearchNewsRequest searchRequest)
    {
        var newsList = await newsRepository.Search(searchRequest);
        return new NewsListResponse(newsList.FoundEntities.Select(x => x.ToResponse()).ToList(), newsList.TotalCount);
    }

    public async Task<NewsResponse> GetNews(ObjectId newsId)
    {
        var news = await newsRepository.Get(newsId);
        return news.ToResponse();
    }

    public async Task<NewsResponse> CreateNews(CreateNewsRequest createNewsRequest, string userId)
    {
        var news = await newsRepository.Create(createNewsRequest, userId);
        return news.ToResponse();
    }

    public async Task PublishNews(DateTime publishTime, ObjectId newsId, string userId)
    {
        var news = await newsRepository.Get(newsId);
        if (news.IsActive)
            throw new CustomException("News already published", 400);
        await newsRepository.Publish(publishTime, newsId, userId);
    }

    public async Task UpdateNews(UpdateNewsRequest updateNewsRequest, ObjectId newsId, string userId) => 
        await newsRepository.Update(updateNewsRequest, newsId, userId);

    public async Task DeleteNews(ObjectId newsId) => 
        await newsRepository.Delete(newsId);

    
    //костыль
    
    public async Task<NewsListResponse> SearchPublished(SearchNewsRequest searchRequest)
    {
        searchRequest.NewsType = NewsType.Published;
        var news = await Search(searchRequest);

        var newsIds = news.FoundEntities.Select(x => ObjectId.Parse(x.Id)).ToList();
        await newsRepository.AddPreviews(newsIds);
        
        return news;
    }

    public async Task<NewsResponse> GetPublishedNews(ObjectId newsId)
    {
        var news = await newsRepository.Get(newsId);
        if (!news.IsPublished())
            throw new CustomException("News not found", 404);
        
        await newsRepository.AddView(news);
        
        return news.ToResponse();
    }
}