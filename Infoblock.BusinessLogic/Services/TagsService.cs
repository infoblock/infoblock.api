﻿using Infoblock.BusinessLogic.Repositories;

namespace Infoblock.BusinessLogic.Services;

public class TagsService : ITagsService
{
    private readonly ITagsRepository tagsRepository;

    public TagsService(ITagsRepository tagsRepository)
    {
        this.tagsRepository = tagsRepository;
    }
    
    public async Task<IList<string>> Search(string tagName)
    {
        var tags = await tagsRepository.Search(tagName);
        return tags.FoundEntities.Select(x => x.Name).ToList();
    }

    public async Task<string> Add(string tagName)
    {
        var tag = await tagsRepository.TryGet(tagName);
        if (tag != null)
            await tagsRepository.UpdateCounter(tag.Id, tag.Count + 1);
        else
            await tagsRepository.Add(tagName);
        return tagName;
    }

    public async Task Delete(string tagName)
    {
        var tag = await tagsRepository.TryGet(tagName);
        if (tag == null) return;
        if (tag.Count > 1)
            await tagsRepository.UpdateCounter(tag.Id, tag.Count - 1);
        else 
            await tagsRepository.Delete(tag.Id);
    }
}