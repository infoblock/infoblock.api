﻿namespace Infoblock.BusinessLogic.Services;

public interface ITagsService
{
    Task<IList<string>> Search(string? tagName);
    Task<string> Add(string tagName);
    Task Delete(string tagName);
}