﻿using Infoblock.Data.Api.Responses;

namespace Infoblock.BusinessLogic.Services;

public interface IUsersService
{
    Task<AuthResponse> Login(string email, string password);
    Task<AuthResponse> RefreshToken(string token);
    Task<AuthResponse> Register(string email, string password, string token);
    Task InviteUser(string email);
    Task<UsersResponse> GetUsers(bool? enabled, string userId);
    Task ChangeUserStatus(string userId, bool enabled);
    Task<InvitesResponse> GetInvites();
}