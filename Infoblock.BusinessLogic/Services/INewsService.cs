﻿using Infoblock.Data.Api.Requests;
using Infoblock.Data.Api.Responses;
using MongoDB.Bson;

namespace Infoblock.BusinessLogic.Services;

public interface INewsService
{
    Task<NewsListResponse> Search(SearchNewsRequest searchRequest);
    Task<NewsResponse> GetNews(ObjectId newsId);
    Task<NewsResponse> CreateNews(CreateNewsRequest createNewsRequest, string userId);
    Task PublishNews(DateTime publishTime, ObjectId newsId, string userId);
    Task UpdateNews(UpdateNewsRequest updateNewsRequest, ObjectId newsId, string userId);
    Task DeleteNews(ObjectId newsId);
    
    //костыль
    Task<NewsListResponse> SearchPublished(SearchNewsRequest searchRequest);
    Task<NewsResponse> GetPublishedNews(ObjectId newsId);
}