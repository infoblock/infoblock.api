﻿using System.Globalization;
using System.Net.Http.Headers;
using FS.Keycloak.RestApiClient.Api;
using FS.Keycloak.RestApiClient.Client;
using FS.Keycloak.RestApiClient.Model;
using Infoblock.BusinessLogic.Mappers;
using Infoblock.BusinessLogic.Repositories;
using Infoblock.BusinessLogic.Spam;
using Infoblock.Data.Api.Responses;
using Infoblock.Data.Configuration;
using Infoblock.Data.Exceptions;
using Newtonsoft.Json.Linq;
using Vostok.Configuration.Abstractions;
using Vostok.Logging.Abstractions;

namespace Infoblock.BusinessLogic.Services;

public class UsersService : IUsersService
{
    private readonly ILog log;
    private readonly UsersApi usersApi;
    private readonly UserApi userApi;
    private readonly KeycloackSettings keycloackSettings;
    private readonly ServiceProperties serviceProperties;
    private readonly SmtpSettings smtpSettings;
    private readonly INewsRepository newsRepository;
    private readonly ISpamService spamService;
    private readonly HttpClient httpClient;

    public UsersService(KeycloakHttpClient keycloakHttpClient, IConfigurationProvider configurationProvider, 
        INewsRepository newsRepository, ILog log, ISpamService spamService)
    {
        this.newsRepository = newsRepository;
        this.log = log;
        this.spamService = spamService;
        httpClient = new HttpClient();
        keycloackSettings = configurationProvider.Get<KeycloackSettings>();
        serviceProperties = configurationProvider.Get<ServiceProperties>();
        smtpSettings = configurationProvider.Get<SmtpSettings>();
        usersApi = ApiClientFactory.Create<UsersApi>(keycloakHttpClient);
        userApi = ApiClientFactory.Create<UserApi>(keycloakHttpClient);
    }

    public async Task<AuthResponse> Login(string email, string password)
    {
        var response = await AuthUser(email, password);
        return response;
    }

    public async Task<AuthResponse> RefreshToken(string token)
    {
        var response = await AuthUser(token: token);
        return response;
    }

    public async Task<AuthResponse> Register(string email, string password, string token)
    {
        log.Info($"Check user {email} for registration");
        var userResponse = (await usersApi.GetUsersAsync(keycloackSettings.RealmName, email: email)).FirstOrDefault();
        
        if (userResponse == null)
        {
            log.Info($"Invite for email: '{email}' not found");
            throw new CustomException("Invite not found", 404);
        }
        if (userResponse.Id != token)
        {
            log.Info($"Token for email: '{email}' not valid");
            throw new CustomException($"Token for email: '{email}' not valid", 403);
        }
        if (userResponse.EmailVerified ?? false)
        {
            log.Info($"User: '{email}' is already registered");
            throw new CustomException($"User: '{email}' is already registered", 400);
        }
        var inviteExpireIn =  DateTime.UtcNow - DateTime.Parse(userResponse.Attributes[keycloackSettings.Attributes.InviteTime].Last()).Add(keycloackSettings.InviteExpireIn);
        if (inviteExpireIn.TotalMinutes > 0)
        {
            log.Info($"Invite for email: '{email}' has expired");
            throw new CustomException("Invite lifetime has expired", 403);
        }
        
        userResponse.Credentials = new List<CredentialRepresentation> { new() { Type = "password", Value = password}};
        await userApi.PutUsersByIdAsync(keycloackSettings.RealmName, userResponse.Id, new UserRepresentation
        {
            Enabled = true, EmailVerified = true, Credentials = userResponse.Credentials
        });
            
        log.Info($"User: '{email}' registered");
        return await AuthUser(email, password);
    }

    public async Task InviteUser(string email)
    {
        log.Info($"Check user {email} for registration");
        var userResponse = (await usersApi.GetUsersAsync(keycloackSettings.RealmName, email: email)).FirstOrDefault();
        if (userResponse != null && (userResponse.EmailVerified ?? false))
        {
            log.Warn($"User: '{email}' already registered");
            throw new CustomException("User already registered", 400);
        }
        
        if (userResponse == null)
        {
            var inviteTime = new Dictionary<string, List<string>> { { keycloackSettings.Attributes.InviteTime, new List<string> { DateTime.UtcNow.ToString(CultureInfo.InvariantCulture) } } };
            await usersApi.PostUsersAsync(keycloackSettings.RealmName, new UserRepresentation { Email = email, Username = email, Attributes = inviteTime});
        }

        log.Info($"Get user with email: {email}");
        var user = (await usersApi.GetUsersAsync(keycloackSettings.RealmName, email: email)).First();

        var nextInviteTime = DateTime.Parse(user.Attributes[keycloackSettings.Attributes.InviteTime].Last()).Add(smtpSettings.EmailFrequency) - DateTime.UtcNow;
        if (userResponse != null && nextInviteTime.TotalMinutes > 0)
            throw new CustomException($"Next invitation can be sent only after {(int)nextInviteTime.TotalMinutes} minutes", 400);
        
        log.Info($"Send invite to: {email}");
        var body = GenerateRegistrationMessage(user.Email, user.Id);
        await spamService.SendMessage(email, smtpSettings.InviteSettings.Subject, body, smtpSettings.InviteSettings.IsBodyHtml);
        
        log.Info($"Update invite for: {email}");
        user.Attributes[keycloackSettings.Attributes.InviteTime].Add(DateTime.UtcNow.ToString(CultureInfo.InvariantCulture));
        await userApi.PutUsersByIdAsync(keycloackSettings.RealmName, user.Id, new UserRepresentation(attributes: user.Attributes)); 
    }

    public async Task<UsersResponse> GetUsers(bool? enabled, string userId)
    {
        var usersResponse = await usersApi.GetUsersAsync(keycloackSettings.RealmName, enabled: enabled, emailVerified: true);
        
        var usersToNewsCounts = new Dictionary<string, int>();
        foreach (var user in usersResponse)
        {
            var count = await newsRepository.GetUserNewsCount(user.Id);
            usersToNewsCounts.Add(user.Id, count);
        }
        
        return new UsersResponse(
            usersResponse
                .Where(x => x.Id != userId)
                .Select(user => user.ToResponse(usersToNewsCounts[user.Id]))
                .ToList(), 
            usersResponse.Count);
    }

    public async Task ChangeUserStatus(string userId, bool enabled)
    {
        var userResponse = (await usersApi.GetUsersAsync(keycloackSettings.RealmName, idpUserId: userId)).FirstOrDefault();
        if (userResponse != null)
            throw new CustomException("User not found", 404);

        await userApi.PutUsersByIdAsync(keycloackSettings.RealmName, userId, new UserRepresentation(enabled: enabled));
        
        log.Info($"User: '{userId}' is active={enabled}");
    }

    public async Task<InvitesResponse> GetInvites()
    {
        var invitesResponse = await usersApi.GetUsersAsync(keycloackSettings.RealmName, emailVerified: false);
    
        return new InvitesResponse(
            invitesResponse
                .Select(invite => invite.ToInviteResponse())
                .ToList(), 
            invitesResponse.Count);
    }

    private string GenerateRegistrationMessage(string email, string id)
    {
        var link =  $"{serviceProperties.FrontRegisterUrl}?token={id}&email={email}";
        var body = smtpSettings.InviteSettings.Body.Replace(smtpSettings.InviteSettings.LinkPlace, link);
        return body;
    }

    
    //TODO: сделать через нормальный метод клиента
    private async Task<AuthResponse> AuthUser(string? username = null, string? password = null, string? token = null)
    {
        httpClient.DefaultRequestHeaders.Accept.Clear();
        httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        var parameters = new Dictionary<string, string> { {"client_id", keycloackSettings.ClientId}, {"client_secret", keycloackSettings.ClientSecret} };
        
        if (username != null && password != null)
        {
            parameters.Add("username", username);
            parameters.Add("password", password);
            parameters.Add("grant_type", "password");
        }
        if (token != null)
        {
            parameters.Add("refresh_token", token);
            parameters.Add("grant_type", "refresh_token");
        }
        
        var content = new FormUrlEncodedContent(parameters);

        log.Info($"Send login request for: '{username}'");
        var response = await httpClient.PostAsync(keycloackSettings.AuthUrl, content);
        var result = await response.Content.ReadAsStringAsync();
        var jsonResult = JObject.Parse(result);
        
        if (!response.IsSuccessStatusCode)
        {
            log.Warn($" Login error for user: '{username}'");
            throw new CustomException(jsonResult["error_description"]?.ToString() ?? "", (int)response.StatusCode);
        }

        log.Info($"User with email: '{username ?? "TOKEN"}' login");
        return new AuthResponse()
        {
            AccessToken = jsonResult["access_token"]?.ToString() ?? "",
            ExpiresIn = (int)(jsonResult["expires_in"] ?? 0),
            RefreshToken = jsonResult["refresh_token"]?.ToString() ?? "",
            RefreshExpiresIn = (int)(jsonResult["refresh_expires_in"] ?? 0),
            SessionState = jsonResult["session_state"]?.ToString() ?? ""
        };
    }
}