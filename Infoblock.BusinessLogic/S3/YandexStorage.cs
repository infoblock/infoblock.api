﻿using Amazon.S3;
using Amazon.S3.Model;
using Infoblock.Data.Configuration;
using Microsoft.AspNetCore.Http;
using Vostok.Configuration.Abstractions;
using Vostok.Logging.Abstractions;

namespace Infoblock.BusinessLogic.S3;

public class YandexStorage : IS3Storage
{
    private readonly ILog log;
    private readonly AmazonS3Client s3Client;
    private readonly StorageSettings storageSettings;

    public YandexStorage(ILog log, AmazonS3Client s3Client, IConfigurationProvider configurationProvider)
    {
        this.log = log;
        this.s3Client = s3Client;
        storageSettings = configurationProvider.Get<StorageSettings>();
    }
    
    public async Task<string> UploadFile(IFormFile file)
    {
        var fileName = Guid.NewGuid() + "_" + file.FileName;
        var request = new PutObjectRequest
        {
            BucketName = storageSettings.BucketName,
            Key = storageSettings.RootPath + fileName,
            InputStream = file.OpenReadStream(),
            ContentType = file.ContentType
        };

        await s3Client.PutObjectAsync(request);
        log.Info($"Successfully uploaded file: '{fileName}'");
        
        var fileUrl = storageSettings.FilesBaseUrl + fileName;
        return fileUrl;
    }
}
