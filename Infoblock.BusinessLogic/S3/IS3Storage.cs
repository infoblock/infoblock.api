﻿using Microsoft.AspNetCore.Http;

namespace Infoblock.BusinessLogic.S3;

public interface IS3Storage
{
    Task<string> UploadFile(IFormFile file);
}