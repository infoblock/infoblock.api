FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["Infoblock.Api/Infoblock.Api.csproj", "Infoblock.Api/"]
RUN dotnet restore "Infoblock.Api/Infoblock.Api.csproj"
COPY . .
WORKDIR "/src/Infoblock.Api"
RUN dotnet build "Infoblock.Api.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Infoblock.Api.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Infoblock.Api.dll"]
